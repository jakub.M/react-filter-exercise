import { FILTER_QUERY_CHANGE }from '../types/people';

export const filterQueryChange = (e) => ({
  type: FILTER_QUERY_CHANGE,
  payload: e.target.value,
});
