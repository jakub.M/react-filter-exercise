import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FilterInput from './FilterInput';
import { filterQueryChange } from '../actions/people';

const Filter = ({ filterQuery, filterQueryChange }) => (
  <FilterInput value={filterQuery} filterQueryChange={filterQueryChange} />
);

Filter.propTypes = {
  filterQuery: PropTypes.string.isRequired,
  filterQueryChange: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  filterQuery: state.peopleReducer.filterQuery,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  filterQueryChange,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
