import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PeopleList from './PeopleList';

const People = ({ people }) => (
  <PeopleList people={people} />
);

People.propTypes = {
  people: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  people: state.peopleReducer.filteredPeople,
});

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(People);
