import React from 'react';
import PropTypes from 'prop-types';

const FilterInput = ({ value, filterQueryChange }) => (
  <input
    className="App-box"
    type="text"
    placeholder="Search..."
    value={value}
    onChange={filterQueryChange}
  />
);

FilterInput.propTypes = {
  value: PropTypes.string.isRequired,
  filterQueryChange: PropTypes.func.isRequired,
};

export default FilterInput;
