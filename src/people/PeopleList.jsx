import React from 'react';
import PropTypes from 'prop-types';

const PeopleList = ({ people }) => (
  <div>
    {people.map(man => (
      <div className="App-box" key={man.id}>{man.name}</div>
    ))}
  </div>
);

PeopleList.propTypes = {
  people: PropTypes.array.isRequired,
};

export default PeopleList;
